//
//  ViewController.swift
//  SwiftOne
//
//  Created by Teddy Li on 11/26/15.
//  Copyright © 2015 TXL Projects. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txt_Edit1: UITextField!
    @IBOutlet weak var lbl_label1: UILabel!
    @IBOutlet weak var lbl_label2: UILabel!
    @IBOutlet weak var btn_num1: UIButton!
    @IBOutlet weak var btn_num2: UIButton!
    @IBOutlet weak var btn_num3: UIButton!
    @IBOutlet weak var btn_reset: UIButton!
    
    
    var Name: String = "thetli8"
    var year: Int = 2015
    
    @IBAction func btn_num1_action(sender: UIButton) {
        lbl_label2.text = "Run num1"
        btn_num1.setTitle("Ran 1", forState: UIControlState.Normal)
        btn_num1.enabled = false //when you click num1, can't click again
        btn_reset.enabled = true //reset is available
        if Int(txt_Edit1.text!) == year{
            lbl_label1.text = "It IS \(year)!"
        }
        else if txt_Edit1.text == Name{
            lbl_label1.text = "I am Teddy!"
        }
        else{
            lbl_label1.text = "Why not try 2015 or thetli8"
        }
        
    }
    @IBAction func btn_num2_action(sender: UIButton) {
        lbl_label2.text = "Run num2"
        btn_num2.setTitle("Ran 2", forState: UIControlState.Normal)
        btn_num2.enabled = false //when you click num2, can't click again
        btn_reset.enabled = true //reset is available
        btn_reset.setTitle("Run Reset", forState: UIControlState.Normal)
    }
    @IBAction func btn_num3_action(sender: UIButton) {
        chgText() //calls chgText() function
    }
    @IBAction func btn_reset_action(sender: UIButton) {
        reset() //calls reset() function
    }
    
    func chgText(){
        lbl_label2.text = "Run chgTxt()"
        btn_num3.setTitle("Ran 3", forState: UIControlState.Normal)
        btn_num3.enabled = false //when you click num3, can't click again
        btn_reset.enabled = true //reset is available
        btn_reset.setTitle("Run Reset", forState: UIControlState.Normal)
    }
    func startText(){
        lbl_label1.text = "Text Output"
        lbl_label2.text = "Output"
        btn_reset.setTitle("Reset", forState: UIControlState.Normal)
        btn_num1.setTitle("Ok", forState: UIControlState.Normal)
        btn_num2.setTitle("Start 2", forState: UIControlState.Normal)
        btn_num3.setTitle("Start 3", forState: UIControlState.Normal)
        btn_reset.enabled = false //you can't reset if nothing's happened
    }
    func reset(){
        startText()
        btn_reset.setTitle("Ran Reset", forState: UIControlState.Normal)
        btn_num1.enabled = true
        btn_num2.enabled = true
        btn_num3.enabled = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startText()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

